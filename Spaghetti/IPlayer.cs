﻿//-----------------------------------------------------------------------
// <copyright file="IPlayer.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    /// <summary>
    /// The user which connects to the game.
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>The connection.</value>
        Connection Connection { get; }
    }
}