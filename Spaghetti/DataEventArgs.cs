﻿//-----------------------------------------------------------------------
// <copyright file="DataEventArgs.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Diagnostics.Contracts;
    using Windows.Storage.Streams;

    /// <summary>
    /// Default event arguments used by <see cref="Core"/> being made up of jut a buffer and the connection that sent it.
    /// </summary>
    public class DataEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataEventArgs"/> class.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        public DataEventArgs(IBuffer buffer)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);

            Buffer = buffer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataEventArgs"/> class.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="connection">The connection.</param>
        public DataEventArgs(IBuffer buffer, Connection connection)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);
            Contract.Requires(connection != default(Connection));

            Buffer = buffer;
            Connection = connection;
        }

        /// <summary>
        /// Gets the buffer.
        /// </summary>
        /// <value>
        /// The buffer.
        /// </value>
        public IBuffer Buffer { get; }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        public Connection Connection { get; }
    }
}
