//-----------------------------------------------------------------------
// <copyright file="DataStreamExtensions.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System.Diagnostics.Contracts;
    using Windows.Storage.Streams;

    /// <summary>
    /// Extensions for <see cref="DataWriter"/> and <see cref="DataReader"/>.
    /// </summary>
    public static class DataStreamExtensions
    {
        /// <summary>
        /// Writes the string with byte as the length.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        public static void WriteStringWithByteLength(this DataWriter writer, string value)
        {
            Contract.Requires(writer != null);

            if (string.IsNullOrEmpty(value))
                writer.WriteByte(0);
            else
            {
                writer.WriteByte((byte)writer.MeasureString(value));
                writer.WriteString(value);
            }
        }

        /// <summary>
        /// Reads the string with byte as the length.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The string.</returns>
        public static string ReadStringWithByteLength(this DataReader reader)
        {
            Contract.Requires(reader != null);

            var length = reader.ReadByte();
            return reader.ReadString(length);
        }

        /// <summary>
        /// Writes the string with <see cref="ushort"/> as the length.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        public static void WriteStringWithUShortLength(this DataWriter writer, string value)
        {
            Contract.Requires(writer != null);

            if (string.IsNullOrEmpty(value))
                writer.WriteUInt16(0);
            else
            {
                writer.WriteUInt16((ushort)writer.MeasureString(value));
                writer.WriteString(value);
            }
        }

        /// <summary>
        /// Reads the string with <see cref="ushort"/> as the length.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The string.</returns>
        public static string ReadStringWithUShortLength(this DataReader reader)
        {
            Contract.Requires(reader != null);

            var length = reader.ReadUInt16();
            return reader.ReadString(length);
        }

        /// <summary>
        /// Writes the string with <see cref="uint"/> as the length.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        public static void WriteStringWithUIntLength(this DataWriter writer, string value)
        {
            Contract.Requires(writer != null);

            if (string.IsNullOrEmpty(value))
                writer.WriteUInt32(0);
            else
            {
                writer.WriteUInt32(writer.MeasureString(value));
                writer.WriteString(value);
            }
        }

        /// <summary>
        /// Reads the string with <see cref="uint"/> as the length.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The string.</returns>
        public static string ReadStringWithUIntLength(this DataReader reader)
        {
            Contract.Requires(reader != null);

            var length = reader.ReadUInt32();
            return reader.ReadString(length);
        }
    }
}
