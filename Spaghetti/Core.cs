﻿//-----------------------------------------------------------------------
// <copyright file="Core.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Threading.Tasks;
    using Windows.Storage.Streams;

    /// <summary>
    /// First byte of every message determining how the message should be handled.
    /// </summary>
    public enum MessageId : byte
    {
        /// <summary>
        /// New player has connected.
        /// </summary>
        Connect,

        /// <summary>
        /// Player has disconnected.
        /// </summary>
        Disconnect,

        /// <summary>
        /// Server or client data.
        /// </summary>
        Data,

        /// <summary>
        /// Server or client updates.
        /// </summary>
        Update,
    }

    /// <summary>
    /// Provides core methods for networking.
    /// </summary>
    public abstract class Core
    {
        /// <summary>
        /// Dictionary of players with their corresponding ids.
        /// </summary>
        private readonly Dictionary<int, IPlayer> playerIds = new Dictionary<int, IPlayer>();

        /// <summary>
        /// Occurs when a message is received.
        /// </summary>
        public event EventHandler<DataEventArgs> ReceiveEvent = delegate { };

        /// <summary>
        /// Occurs when a client connects.
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ConnectEvent = delegate { };

        /// <summary>
        /// Occurs when a client disconnects.
        /// </summary>
        public event EventHandler<ConnectionEventArgs> DisconnectEvent = delegate { };

        /// <summary>
        /// Occurs when data is received.
        /// </summary>
        public event EventHandler<DataEventArgs> DataEvent = delegate { };

        /// <summary>
        /// Occurs when update data is received.
        /// </summary>
        public event EventHandler<DataEventArgs> UpdateEvent = delegate { };

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Core"/> is running.
        /// </summary>
        /// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
        public bool Running { get; set; }

        /// <summary>
        /// Gets the players.
        /// </summary>
        /// <value>The players.</value>
        public IReadOnlyDictionary<int, IPlayer> Players
        {
            get
            {
                return playerIds;
            }
        }

        /// <summary>
        /// Sends the specified buffer with the message id.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="connection">The connection.</param>
        /// <returns>The task.</returns>
        public async virtual Task Send(MessageId messageId, IBuffer buffer, Connection connection)
        {
            try
            {
                using (var writer = new DataWriter(connection.OutputStream))
                {
                    // 1 byte for the message id
                    writer.WriteByte((byte)messageId);

                    // write the buffer
                    writer.WriteBuffer(buffer);

                    // store to the underlying stream
                    await writer.StoreAsync();

                    // detach the stream so that the stream doesn't get closed
                    writer.DetachStream();
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"[{GetType().Name}] {exception}", "Error");
            }
        }

        /// <summary>
        /// Raises the <see cref="ConnectEvent"/> event.
        /// </summary>
        /// <param name="args">
        /// The <see cref="ConnectionEventArgs"/> instance containing the event data.
        /// </param>
        public virtual void OnConnectEvent(ConnectionEventArgs args)
        {
            Contract.Requires(args != null);

            ConnectEvent(this, args);
        }

        /// <summary>
        /// Raises the <see cref="DisconnectEvent"/> event.
        /// </summary>
        /// <param name="args">
        /// The <see cref="ConnectionEventArgs"/> instance containing the event data.
        /// </param>
        public virtual void OnDisconnectEvent(ConnectionEventArgs args)
        {
            Contract.Requires(args != null);

            DisconnectEvent(this, args);
        }

        /// <summary>
        /// Raises the <see cref="DataEvent"/> event.
        /// </summary>
        /// <param name="args">The <see cref="DataEventArgs"/> instance containing the event data.</param>
        public virtual void OnDataEvent(DataEventArgs args)
        {
            Contract.Requires(args != null);

            DataEvent(this, args);
        }

        /// <summary>
        /// Raises the <see cref="UpdateEvent"/> event.
        /// </summary>
        /// <param name="args">The <see cref="DataEventArgs"/> instance containing the event data.</param>
        public virtual void OnUpdateEvent(DataEventArgs args)
        {
            Contract.Requires(args != null);

            UpdateEvent(this, args);
        }

        /// <summary>
        /// Adds the player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>The id of the player added or -1 if the player could not be added.</returns>
        protected virtual int AddPlayer(IPlayer player)
        {
            Contract.Requires(player != null);

            // find taken ids
            var takenIds = new bool[playerIds.Count + 1];

            foreach (var playerId in playerIds)
                takenIds[playerId.Key] = true;

            // add the connecting players
            for (byte i = 0; i < takenIds.Length; i++)
            {
                if (takenIds[i] == false)
                {
                    playerIds.Add(i, player);
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Adds the player with the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="player">The player.</param>
        /// <returns>The id of the player added or -1 if the player could not be added.</returns>
        protected virtual int AddPlayer(int id, IPlayer player)
        {
            Contract.Requires(player != null);

            // remove any players with the id
            if (playerIds.Remove(id))
            {
                // add the new player
                playerIds.Add(id, player);
                return id;
            }

            return -1;
        }

        /// <summary>
        /// Removes the player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>The id of the player removed or -1 if the player could not be removed.</returns>
        protected virtual int RemovePlayer(IPlayer player)
        {
            // ensure the player is one of our players
            if (!playerIds.ContainsValue(player))
                return -1;

            var id = playerIds.FirstOrDefault(p => p.Value == player).Key;
            playerIds.Remove(id);

            return id;
        }

        /// <summary>
        /// Removes the players of the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns>The ids of the players removed.</returns>
        protected virtual int[] RemovePlayers(Connection connection)
        {
            Contract.Requires(connection != default(Connection));

            var players = playerIds.Values.Where(p => p.Connection == connection).ToList();

            var ids = new List<int>();
            foreach (var player in players)
                ids.Add(RemovePlayer(player));

            return ids.ToArray();
        }

        /// <summary>
        /// Raises the <see cref="ReceiveEvent"/> event.
        /// </summary>
        /// <param name="args">The <see cref="DataEventArgs"/> instance containing the event data.</param>
        protected void OnReceiveEvent(DataEventArgs args)
        {
            Contract.Requires(args != null);
            Contract.Requires(args.Buffer != null && args.Buffer.Length > 0);
            Contract.Requires(args.Connection != default(Connection));

            using (var reader = DataReader.FromBuffer(args.Buffer))
            {
                switch ((MessageId)reader.ReadByte())
                {
                    case MessageId.Connect:
                        OnConnectEvent(new ConnectionEventArgs(args.Buffer, args.Connection));
                        break;

                    case MessageId.Disconnect:
                        OnDisconnectEvent(new ConnectionEventArgs(args.Buffer, args.Connection));
                        break;

                    case MessageId.Data:
                        OnDataEvent(new DataEventArgs(args.Buffer, args.Connection));
                        break;

                    case MessageId.Update:
                        OnUpdateEvent(new DataEventArgs(args.Buffer, args.Connection));
                        break;
                    default:
                        Debug.WriteLine("Bad MessageId received", "Error");
                        return;
                }
            }

            ReceiveEvent(this, args);
        }
    }
}