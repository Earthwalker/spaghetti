﻿//-----------------------------------------------------------------------
// <copyright file="IServer.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Threading.Tasks;
    using Windows.Storage.Streams;

    /// <summary>
    /// Interface for the server-side of networking.
    /// </summary>
    [ContractClass(typeof(IServerContract))]
    public interface IServer : INetworking
    {
        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        IReadOnlyList<Connection> Connections { get; }

        /// <summary>
        /// Starts listening at the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        Task Start(ushort port);

        /// <summary>
        /// Sends the specified buffer with the message id.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="connection">The connection.</param>
        /// <returns>The task.</returns>
        Task Send(MessageId messageId, IBuffer buffer, Connection connection);
    }

    /// <summary>
    /// Contract class for <see cref="IServer"/>.
    /// </summary>
    [ContractClassFor(typeof(IServer))]
    internal abstract class IServerContract : IServer
    {
        /// <summary>
        /// Occurs when a message is received.
        /// </summary>
        public abstract event EventHandler<DataEventArgs> ReceiveEvent;

        /// <summary>
        /// Gets the players and their ids.
        /// </summary>
        /// <value>
        /// The players and their ids.
        /// </value>
        public abstract IReadOnlyDictionary<int, IPlayer> Players { get; }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        public abstract IReadOnlyList<Connection> Connections { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IServer"/> is running.
        /// </summary>
        /// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
        public abstract bool Running { get; set; }

        /// <summary>
        /// Starts listening at the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        public Task Start(ushort port)
        {
            Contract.Ensures(Running);

            return default(Task);
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Sends the specified buffer.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// The task.
        /// </returns>
        public Task Send(MessageId messageId, IBuffer buffer, Connection connection)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);
            Contract.Requires(connection != default(Connection));

            return default(Task);
        }
    }
}