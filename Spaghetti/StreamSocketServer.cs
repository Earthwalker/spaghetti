﻿//-----------------------------------------------------------------------
// <copyright file="StreamSocketServer.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Windows.Networking.Sockets;
    using Windows.Storage.Streams;

    /// <summary>
    /// Basic TCP server.
    /// </summary>
    public class StreamSocketServer : ServerBase
    {
        /// <summary>
        /// The listener.
        /// </summary>
        private StreamSocketListener listener;

        /// <summary>
        /// The input stream.
        /// </summary>
        private IInputStream inputStream;

        /// <summary>
        /// The output stream.
        /// </summary>
        private IOutputStream outputStream;

        /// <summary>
        /// Starts listening at the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        public async override Task Start(ushort port)
        {
            try
            {
                listener = new StreamSocketListener();

                // bind the event for when a player connects
                listener.ConnectionReceived += OnConnectionReceived;

                // bind on the specified port
                await listener.BindServiceNameAsync(port.ToString());

                Running = true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"[{GetType().Name}] {exception}", "Error");
                return;
            }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public override void Close()
        {
            listener?.Dispose();
            listener = null;

            Running = false;
        }

        /// <summary>
        /// Called when a connection is received.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">
        /// The <see cref="StreamSocketListenerConnectionReceivedEventArgs"/> instance containing
        /// the event data.
        /// </param>
        private async void OnConnectionReceived(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
        {
            inputStream = args.Socket.InputStream;
            outputStream = args.Socket.OutputStream;

            var connection = new Connection(args.Socket.Information.RemoteHostName, outputStream);

            using (var reader = new DataReader(inputStream))
            {
                while (listener != null)
                {
                    // set input stream options so that we don't have to know the data size
                    reader.InputStreamOptions = InputStreamOptions.Partial;

                    try
                    {
                        // read new data
                        await reader.LoadAsync(1024);

                        // call the receive event
                        OnReceiveEvent(new DataEventArgs(reader.ReadBuffer(reader.UnconsumedBufferLength), connection));
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine($"[{GetType().Name}] {exception}", "Error");
                    }
                }
            }
        }
    }
}