﻿//-----------------------------------------------------------------------
// <copyright file="ConnectionEventArgs.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Diagnostics.Contracts;
    using Windows.Storage.Streams;

    /// <summary>
    /// Event arguments used by <see cref="Core"/>.
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEventArgs" /> class.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="player">The player.</param>
        /// <param name="id">The identifier.</param>
        public ConnectionEventArgs(IBuffer buffer, IPlayer player, byte id = 0)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);
            Contract.Requires(player != null);

            Buffer = buffer;
            Player = player;
            Id = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEventArgs" /> class.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="id">The identifier.</param>
        public ConnectionEventArgs(IBuffer buffer, Connection connection, byte id = 0)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);
            Contract.Requires(connection != default(Connection));

            Buffer = buffer;
            Connection = connection;
            Id = id;
        }

        /// <summary>
        /// Gets the buffer.
        /// </summary>
        /// <value>The buffer.</value>
        public IBuffer Buffer { get; }

        /// <summary>
        /// Gets the player identifier.
        /// </summary>
        /// <value>
        /// The player identifier.
        /// </value>
        public byte Id { get; }

        /// <summary>
        /// Gets the player.
        /// </summary>
        /// <value>The player.</value>
        public IPlayer Player { get; }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>The connection.</value>
        public Connection Connection { get; }
    }
}