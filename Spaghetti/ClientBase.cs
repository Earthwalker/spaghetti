﻿//-----------------------------------------------------------------------
// <copyright file="ClientBase.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System.Threading.Tasks;
    using Windows.Storage.Streams;

    /// <summary>
    /// Implements <see cref="Core"/> and <see cref="IClient"/>, to provide a default client base.
    /// </summary>
    public abstract class ClientBase : Core, IClient
    {
        /// <summary>
        /// Gets or sets the server connection.
        /// </summary>
        /// <value>The server connection.</value>
        public Connection Server { get; protected set; }

        /// <summary>
        /// Connects to the specified host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        public abstract Task Connect(string host, ushort port);

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Sends the specified buffer with the message id.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <returns>
        /// The task.
        /// </returns>
        public virtual Task Send(MessageId messageId, IBuffer buffer)
        {
            // client can only send to the server
            return Send(messageId, buffer, Server);
        }
    }
}