//-----------------------------------------------------------------------
// <copyright file="INetworking.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Common networking interface for servers and clients.
    /// </summary>
    [ContractClass(typeof(INetworkingContract))]
    public interface INetworking
    {
        /// <summary>
        /// Occurs when a message is received.
        /// </summary>
        event EventHandler<DataEventArgs> ReceiveEvent;

        /// <summary>
        /// Gets the players and their ids.
        /// </summary>
        /// <value>
        /// The players and their ids.
        /// </value>
        IReadOnlyDictionary<int, IPlayer> Players { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IServer"/> is running.
        /// </summary>
        /// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
        bool Running { get; set; }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        void Close();
    }

    /// <summary>
    /// Contract class for <see cref="INetworking"/>.
    /// </summary>
    [ContractClassFor(typeof(INetworking))]
    internal abstract class INetworkingContract : INetworking
    {
        /// <summary>
        /// Occurs when a message is received.
        /// </summary>
        public abstract event EventHandler<DataEventArgs> ReceiveEvent;

        /// <summary>
        /// Gets the players and their ids.
        /// </summary>
        /// <value>
        /// The players and their ids.
        /// </value>
        public abstract IReadOnlyDictionary<int, IPlayer> Players { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IServer"/> is running.
        /// </summary>
        /// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
        public abstract bool Running { get; set; }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public void Close()
        {
            Contract.Ensures(!Running);
        }
    }
}