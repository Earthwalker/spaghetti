﻿//-----------------------------------------------------------------------
// <copyright file="ServerBase.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Windows.Storage.Streams;

    /// <summary>
    /// Implements <see cref="Core"/> and <see cref="IServer"/>, to provide a default server base.
    /// </summary>
    public abstract class ServerBase : Core, IServer
    {
        /// <summary>
        /// Gets or sets the maximum players.
        /// </summary>
        /// <value>The maximum players.</value>
        public int MaxPlayers { get; protected set; }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        public IReadOnlyList<Connection> Connections
        {
            get
            {
                var connections = new List<Connection>();

                foreach (var player in Players)
                {
                    // add the connections if it's a new one
                    if (!connections.Contains(player.Value.Connection))
                        connections.Add(player.Value.Connection);
                }

                return connections;
            }
        }

        /// <summary>
        /// Starts listening at the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        public abstract Task Start(ushort port);

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Adds the player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>
        /// The id of the player added.
        /// </returns>
        protected override int AddPlayer(IPlayer player)
        {
            // make sure we have an open slot
            if (Players.Count >= MaxPlayers)
                return -1;

            return base.AddPlayer(player);
        }

        /// <summary>
        /// Adds the player with the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="player">The player.</param>
        /// <returns>
        /// The id of the player added.
        /// </returns>
        protected override int AddPlayer(int id, IPlayer player)
        {
            // make sure we have an open slot
            if (Players.Count >= MaxPlayers)
                return -1;

            return base.AddPlayer(id, player);
        }
    }
}