﻿//-----------------------------------------------------------------------
// <copyright file="IClient.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Threading.Tasks;
    using Windows.Storage.Streams;

    /// <summary>
    /// Interface for the client-side of networking.
    /// </summary>
    [ContractClass(typeof(IClientContract))]
    public interface IClient : INetworking
    {
        /// <summary>
        /// Gets the server connection.
        /// </summary>
        /// <value>The server connection.</value>
        Connection Server { get; }

        /// <summary>
        /// Connects to the specified host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        Task Connect(string host, ushort port);

        /// <summary>
        /// Sends the specified buffer with the message id.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <returns>The task.</returns>
        Task Send(MessageId messageId, IBuffer buffer);
    }

    /// <summary>
    /// Contract class for <see cref="IClient"/>.
    /// </summary>
    [ContractClassFor(typeof(IClient))]
    internal abstract class IClientContract : IClient
    {
        /// <summary>
        /// Occurs when a message is received.
        /// </summary>
        public abstract event EventHandler<DataEventArgs> ReceiveEvent;

        /// <summary>
        /// Gets the server connection.
        /// </summary>
        /// <value>The server connection.</value>
        public abstract Connection Server { get; }

        /// <summary>
        /// Gets the players and their ids.
        /// </summary>
        /// <value>
        /// The players and their ids.
        /// </value>
        public abstract IReadOnlyDictionary<int, IPlayer> Players { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IClient"/> is running.
        /// </summary>
        /// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
        public abstract bool Running { get; set; }

        /// <summary>
        /// Connects to the specified host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        public Task Connect(string host, ushort port)
        {
            Contract.Requires(!string.IsNullOrEmpty(host));
            Contract.Ensures(Running);

            return default(Task);
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Sends the specified buffer.
        /// </summary>
        /// <param name="messageId">The message identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <returns>The task.</returns>
        public Task Send(MessageId messageId, IBuffer buffer)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);

            return default(Task);
        }
    }
}