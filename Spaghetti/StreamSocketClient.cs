﻿//-----------------------------------------------------------------------
// <copyright file="StreamSocketClient.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Windows.Networking;
    using Windows.Networking.Sockets;
    using Windows.Storage.Streams;

    /// <summary>
    /// Implements <see cref="ClientBase"/>, to provide a basic TCP client.
    /// </summary>
    public class StreamSocketClient : ClientBase, IClient
    {
        /// <summary>
        /// Gets or sets the socket.
        /// </summary>
        /// <value>The socket.</value>
        protected StreamSocket Socket { get; set; }

        /// <summary>
        /// Connects to the specified host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <returns>The task.</returns>
        public async override Task Connect(string host, ushort port)
        {
            try
            {
                Socket = new StreamSocket();

                // try to connect to the host
                await Socket.ConnectAsync(new HostName(host), port.ToString());

                Server = new Connection(Socket.Information.RemoteHostName, Socket.OutputStream);
                Running = true;

                // start receiving data from server
                ListenForMessages();
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"[{GetType().Name}] {exception}", "Error");
                return;
            }
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public async override void Close()
        {
            // if connected, try sending a disconnect message to the server
            if (Server != null)
                await Send(MessageId.Disconnect, null);

            Socket?.Dispose();
            Socket = null;

            Running = false;
        }

        /// <summary>
        /// Listens for new messages.
        /// </summary>
        private async void ListenForMessages()
        {
            using (var reader = new DataReader(Socket.InputStream))
            {
                while (Socket != null)
                {
                    // set input stream options so that we don't have to know the data size
                    reader.InputStreamOptions = InputStreamOptions.Partial;

                    try
                    {
                        // read new data
                        await reader.LoadAsync(1024);

                        OnReceiveEvent(new DataEventArgs(reader.ReadBuffer(reader.UnconsumedBufferLength)));
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine($"[{GetType().Name}] {exception}", "Error");
                        return;
                    }
                }
            }
        }
    }
}