﻿//-----------------------------------------------------------------------
// <copyright file="Connection.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Spaghetti
{
    using System.Diagnostics.Contracts;
    using Windows.Networking;
    using Windows.Storage.Streams;

    /// <summary>
    /// Holds connection data required for networking.
    /// </summary>
    public struct Connection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Connection"/> struct.
        /// </summary>
        /// <param name="hostName">The name of the host.</param>
        /// <param name="outputStream">The output stream.</param>
        public Connection(HostName hostName, IOutputStream outputStream)
        {
            Contract.Requires(hostName != null);
            Contract.Requires(outputStream != null);

            HostName = hostName;
            OutputStream = outputStream;
        }

        /// <summary>
        /// Gets the name of the host.
        /// </summary>
        /// <value>The name of the host.</value>
        public HostName HostName { get; }

        /// <summary>
        /// Gets the output stream.
        /// </summary>
        /// <value>The output stream.</value>
        public IOutputStream OutputStream { get; }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="connection1">The first connection.</param>
        /// <param name="connection2">The second connection.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(Connection connection1, Connection connection2)
        {
            return connection1.Equals(connection2);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="connection1">The first connection.</param>
        /// <param name="connection2">The second connection.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(Connection connection1, Connection connection2)
        {
            return !connection1.Equals(connection2);
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Connection))
                return false;

            return Equals((Connection)obj);
        }

        /// <summary>
        /// Determines whether the specified <see cref="Connection"/>, is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="Connection"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="Connection"/> is equal to this instance;
        /// otherwise, <c>false</c>.
        /// </returns>
        public bool Equals(Connection other)
        {
            return HostName.IsEqual(other.HostName) && OutputStream.Equals(other.OutputStream);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data
        /// structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return new { HostName, OutputStream }.GetHashCode();
        }
    }
}